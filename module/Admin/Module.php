<?php

namespace Admin;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Admin\Model\HotelTable;
use Admin\Model\FotoTable;
use Admin\Model\FotoHotelTable;
use Admin\Model\PaisTable;
use Admin\Model\CiudadTable;
use Admin\View\Helper\FlashMessages;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
                ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                    ),
                ),
            );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);


             //Cargamos algunas librerias
       require_once( APPLICATION_PATH . "/../vendor/ZendImage/zendimage.php");
       include_once( APPLICATION_PATH . "/../vendor/Dom/simple_html_dom.php");

          
   }

   public function getServiceConfig() {
      return array(
        'factories' => array(
            'Admin\Model\HotelTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new HotelTable($dbAdapter);
                return $table;
            },
            'Admin\Model\FotoTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new FotoTable($dbAdapter);
                return $table;
            },
            'Admin\Model\FotoHotelTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new FotoHotelTable($dbAdapter);
                return $table;
            },
            'Admin\Model\PaisTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new PaisTable($dbAdapter);
                return $table;
            },
            'Admin\Model\CiudadTable' => function($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new CiudadTable($dbAdapter);
                
                return $table;
            }
            )
        );
  }


  public function getViewHelperConfig() {
    return array(
        'factories' => array(
            'flashMessages' => function($sm) {
                $flashmessenger = $sm->getServiceLocator()
                ->get('ControllerPluginManager')
                ->get('flashmessenger');

                $messages = new FlashMessages();
                $messages->setFlashMessenger($flashmessenger);

                return $messages;
            }
            ),
        );
}
}
