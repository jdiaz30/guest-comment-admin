<?php
return array(

    'router' => array(
        'routes' => array(
            'home' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),

                    /*'hotel' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/admin/hotel[/:action][/:id][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                               
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\Hotel',
                                'action' => 'index',
                            ),
                        ),
                    ),*/

                    'parametros' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[:controller[/:action][/:id]][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                               
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),

                ),
            ),
        ),
    ),
      'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'admin/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'admin/index/index' => __DIR__ . '/../view/admin/index/index.phtml',
            'header-admin' => __DIR__ . '/../view/layout/partial/header.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',

        ),
        'template_path_stack' => array(
            'admin' => __DIR__ . '/../view',
        )
    ), 
    //Agregar plugins
    'controller_plugins' => array(
        'invokables' => array(
            'codigoPlugin' => 'Admin\Plugin\CodigoPlugin',
            'imagenPlugin' => 'Admin\Plugin\ImagenPlugin',
            'hotelPlugin' => 'Admin\Plugin\HotelPlugin',
            'urlPlugin' => 'Admin\Plugin\UrlPlugin',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Hotel' => 'Admin\Controller\HotelController',
            'Admin\Controller\Config' => 'Admin\Controller\ConfigController',
        ),
    ),
);
