<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\View\Helper\ElementosHtml;

class ConfigController extends AbstractActionController
{
    protected $hotelTable;
  
    public function getHotelTable() {
        if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Admin\Model\HotelTable');
        }
        return $this->hotelTable;
    }

    public function indexAction(){
    
    }

    public function dataFiltrosAction(){

    }

    public function crearDataHotelAction(){
        $dataHotel = $this->getHotelTable()->getHotelFiltro();

        $dataHotelJson = json_encode($dataHotel->toArray());

        $fh = fopen(APPLICATION_PATH ."/data/data-hotel.json", 'w');
        fwrite($fh, $dataHotelJson);
        fclose($fh);

        return $this->getResponse()->setContent("data-hotel.json");

    }

    public function crearDataPaisAction(){
        $dataPais = $this->getHotelTable()->getPaisFiltro();

        $dataPaisJson = json_encode($dataPais->toArray());

        $fh = fopen(APPLICATION_PATH ."/data/data-pais.json", 'w');
        fwrite($fh, $dataPaisJson);
        fclose($fh);

        return $this->getResponse()->setContent("data-pais.json");

    }

    public function crearDataCiudadAction(){
        $dataPais = $this->getHotelTable()->getCiudadFiltro();

        $dataPaisJson = json_encode($dataPais->toArray());

        $fh = fopen(APPLICATION_PATH ."/data/data-ciudad.json", 'w');
        fwrite($fh, $dataPaisJson);
        fclose($fh);

        return $this->getResponse()->setContent("data-ciudad.json");

    }

    public function crearDataLocalidadAction(){
        $dataLocalidad = $this->getHotelTable()->getLocalidadFiltro();

        $dataLocalidadJson = json_encode($dataLocalidad->toArray());

        $fh = fopen(APPLICATION_PATH ."/data/data-localidad.json", 'w');
        fwrite($fh, $dataLocalidadJson);
        fclose($fh);

        return $this->getResponse()->setContent("data-localidad.json");

    }


}
