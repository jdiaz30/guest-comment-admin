<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\View\Helper\ListarImagen;
use Admin\View\Helper\ElementosHtml;

class HotelController extends AbstractActionController
{
    protected $hotelTable;
    protected $fotoTable;
    protected $fotoHotelTable;
    protected $paisTable;
    protected $ciudadTable;

    public function getHotelTable() {
        if (!$this->hotelTable) {
            $sm = $this->getServiceLocator();
            $this->hotelTable = $sm->get('Admin\Model\HotelTable');
        }
        return $this->hotelTable;
    }

    public function getFotoTable() {
        if (!$this->fotoTable) {
            $sm = $this->getServiceLocator();
            $this->fotoTable = $sm->get('Admin\Model\FotoTable');
        }
        return $this->fotoTable;
    }

    public function getFotoHotelTable() {
        if (!$this->fotoHotelTable) {
            $sm = $this->getServiceLocator();
            $this->fotoHotelTable = $sm->get('Admin\Model\FotoHotelTable');
        }
        return $this->fotoHotelTable;
    }

    public function getPaisTable() {
        if (!$this->paisTable) {
            $sm = $this->getServiceLocator();
            $this->paisTable = $sm->get('Admin\Model\PaisTable');
        }
        return $this->paisTable;
    }

    public function getCiudadTable() {
        if (!$this->ciudadTable) {
            $sm = $this->getServiceLocator();
            $this->ciudadTable = $sm->get('Admin\Model\CiudadTable');
        }
        return $this->ciudadTable;
    }

    public function indexAction(){
        $datos=$this->getHotelTable()->getAll();

        return new ViewModel(array(
            'hoteles' => $datos
            ));
    }

    public function addAction(){
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $data['fecha_reg'] = date("Y-m-d H:i:s");

            if(isset($data['foto'])){
                   $dataF= explode(',', $data['foto']);
                   unset($data['foto']);//eliminamos el contenido para que no halla errores al insertar
            }

            unset($data['id_pais']);

            $data['url'] = $this->urlPlugin()->create($data['nombre'],true);

            $this->getHotelTable()->add($data->toArray());
            $this->flashMessenger()->setNamespace('success')->addMessage("Se ha registrado un nuevo hotel");
             
            if(isset($dataF)){
                foreach ($dataF as $foto) {
                    $codigo = $this->codigoPlugin()->codigoAleatorio("foto");

                    $fotoData['id_foto'] = $codigo;
                    $fotoData['foto'] = $foto;
                    $fotoData['estado'] = "1";

                    $this->getFotoTable()->add($fotoData);

                    $fotoHotel['id_foto_hotel'] = $this->codigoPlugin()->codigoAleatorio("foto_hotel");
                    $fotoHotel['id_hotel'] = $data['id_hotel'];
                    $fotoHotel['id_foto'] = $codigo;

                    $this->getFotoHotelTable()->add($fotoHotel);
                }

            }

            return $this->redirect()->toUrl('/hotel/add');
        }else{

            $codigoMax = $this->getHotelTable()->getMaxId();
            $codigo = $this->codigoPlugin()->generarCodigo($codigoMax['total'],"hotel");

            $paisData = $this->getPaisTable()->getAll();

            return new ViewModel(array(
                'codigo'=>$codigo,
                'pais' =>$paisData
                )
            );

        }
    }

    public function updateAction(){

        if($this->getRequest()->isPost()){

            $data = $this->getRequest()->getPost();
            $data['fecha_reg'] = date("Y-m-d H:i:s");

            if(isset($data['foto'])){
                   $dataF= explode(',', $data['foto']);
                   unset($data['foto']);//eliminamos el contenido para que no halla errores al insertar

            }

            if($data['nombre-a']!=$data['nombre']){
                $data['url'] = $this->urlPlugin()->create($data['nombre'],true);
            }

            unset($data['nombre-a']);
            unset($data['id_pais']);
                     
            $this->getHotelTable()->updateHotel($data->toArray());
            $this->flashMessenger()->setNamespace('success')->addMessage("Se ha actualizado los datos del hotel");


            $fHotel = $this->getFotoHotelTable()->getAllHotel($data['id_hotel']);

            $this->getFotoHotelTable()->deleteFoto($data['id_hotel']);

            foreach ($fHotel as $f) {
                    $this->getFotoTable()->deleteFoto($f['id_foto']);
            }
             
            if(isset($dataF)){
   
                foreach ($dataF as $foto) {
                    $codigo = $this->codigoPlugin()->codigoAleatorio("foto");

                    $fotoData['id_foto'] = $codigo;
                    $fotoData['foto'] = $foto;
                    $fotoData['estado'] = "1";

                    $this->getFotoTable()->add($fotoData);

                    $fotoHotel['id_foto_hotel'] = $this->codigoPlugin()->codigoAleatorio("foto_hotel");
                    $fotoHotel['id_hotel'] = $data['id_hotel'];
                    $fotoHotel['id_foto'] = $codigo;

                    $this->getFotoHotelTable()->add($fotoHotel);
                }

            }

            return $this->redirect()->toUrl('/hotel/update/'.$data['id_hotel']);

        }else{

           $id = $this->getEvent()->getRouteMatch()->getParam('id');

           $hotel = $this->getHotelTable()->getAllId($id);
           $fotoHotel = $this->getFotoHotelTable()->getAllHotel($id);

           $paisData = $this->getPaisTable()->getAll();

           $ciudadData = $this->getCiudadTable()->getAllPais($hotel['id_pais']);

           return  new ViewModel(array(
            'hotel'=>$hotel,
            'foto'=>$fotoHotel,
            'pais'=>$paisData,
            'ciudad'=>$ciudadData
            ));
       

        }
    }

    public function uploadImageAction(){
        $dir = APPLICATION_PATH . "/img/hoteles";
        $res = $this->imagenPlugin()->_uploadImage($dir);

        return $this->getResponse()->setContent($res);
    }

    public function urlCommentValidAction(){
            $data=$this->getRequest()->getPost();
    
            $res=$this->hotelPlugin()->validaUrlComment($data['url'],$data['web']);

            return $this->getResponse()->setContent($res);
    }

    public function ajaxCiudadAction(){
        $idPais = $this->getRequest()->getPost('id');

        $ciudadData = $this->getCiudadTable()->getAllPais($idPais);

        $htmlHelperView = new ElementosHtml();

        $selectPais = $htmlHelperView->crearSelect($ciudadData);

        return $this->getResponse()->setContent($selectPais);
    }

    public function promedioAction(){

        $data = $this->getRequest()->getPost();

        $promedio = $this->hotelPlugin()->calculaPromedio($data);

        return $this->getResponse()->setContent($promedio);
    }


}
