<?php

namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class CiudadTable extends AbstractTableGateway {

	protected $table = 'tb_ciudad';

	public function __construct(Adapter $adapter) {
			$this->adapter = $adapter;
	}

	public function getAllPais($idPais){
		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array("c"=>$this->table));
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais",array("name_ciudad"=>"Name"));
		$select->where(array("c.id_pais"=>$idPais));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;

	}
}