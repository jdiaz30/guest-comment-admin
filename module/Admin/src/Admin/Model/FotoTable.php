<?php

namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class FotoTable extends AbstractTableGateway {

	protected $table = 'tb_foto';

	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}

	public function add($datos = array()){
		$this->insert($datos);	
	}

	public function deleteFoto($id) {
        $this->delete(array("id_foto" => $id));
    }

}

?>