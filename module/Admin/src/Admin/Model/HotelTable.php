<?php

namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class HotelTable extends AbstractTableGateway {

	protected $table = 'tb_hotel';

	public function __construct(Adapter $adapter) {
			$this->adapter = $adapter;
	}

	public function getMaxId(){
		$expresion = new Expression("MAX(id_hotel)");
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from($this->table);

		$select->columns(array("total" => $expresion));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$row = $result->current(); //Mostramos solo un registro

		return $row;
	}

	public function getAllId($id){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("h"=>$this->table));
		$select->join(array("c"=>"tb_ciudad"),"c.id_ciudad = h.id_ciudad");
		$select->join(array("p"=>"tb_pais"),"c.id_pais = p.id_pais");

		$select->where(array("h.id_hotel" => $id));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$row = $result->current(); //Mostramos solo un registro

		return $row;
	}

	public function getAllUrl($url){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from($this->table);
		$select->columns(array("booking_url","orbitz_url","despegar_url","hoteles_url","nombre"));

		$select->where(array("url" => $url));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$row = $result->current(); //Mostramos solo un registro

		return $row;
	}

	public function add($datos = array()){
		 $this->insert($datos);
	}

	public function updateHotel($datos = array()){
		 $this->update($datos, array("id_hotel" => $datos['id_hotel']));
	}

	public function getAll(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("a" => $this->table));
		$select->order(array("id_hotel asc"));

	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getAllActive(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("a" => $this->table));
		$select->where(array("a.estado"=>"0"));
		$select->order(array("id_hotel asc"));

	    // $select->columns(array("*"));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getHotelFiltro(){
		$sql = new Sql($this->adapter);
		$select = $sql->select();

		$select->from(array('h' => $this->table));
		$select->columns(array("nombre"));
		$select->join(array('c'=> 'tb_ciudad'),"h.id_ciudad = c.id_ciudad",array('ciudad'=>'Name'));
		$select->join(array('p'=> 'tb_pais'),"c.id_pais = p.id_pais",array('pais'=>'Name'));

        $select->where
			->nest
			->and->equalTo('h.estado', 0)
			->and->notEqualTo("h.booking_url","")
			/*->and->notEqualTo("h.hoteles_url","")
			->and->notEqualTo("h.expedia_url","")*/
			->and->notEqualTo("h.mapa","")
			->and->notEqualTo("h.puntaje","");

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getPaisFiltro(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('p'=> 'tb_pais'));
		$select->columns(array('value'=>'Name'));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getCiudadFiltro(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('c'=> 'tb_ciudad'));
		$select->columns(array('ciudad'=>'Name'));

		$select->join(array('p'=> 'tb_pais'),"c.id_pais = p.id_pais",array('pais'=>'Name'));

		$select->quantifier('DISTINCT');

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

	public function getLocalidadFiltro(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();

		$select->from(array('a' => $this->table));
		$select->columns(array("localidad"));

		$select->join(array('c'=> 'tb_ciudad'),"a.id_ciudad = c.id_ciudad",array('ciudad'=>'Name'));
		$select->join(array('p'=> 'tb_pais'),"c.id_pais = p.id_pais",array('pais'=>'Name'));

		$select->quantifier('DISTINCT');

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

	    //Mostramos todos los registros
		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}


}