<?php

namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class PaisTable extends AbstractTableGateway {

	protected $table = 'tb_pais';

	public function __construct(Adapter $adapter) {
			$this->adapter = $adapter;
	}

	public function getAll(){
		$sql = new Sql($this->adapter);

		$select = $sql->select();
		$select->from(array("p"=>$this->table));

		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		return $resultSet;
	}

}


?>