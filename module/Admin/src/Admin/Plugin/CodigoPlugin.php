<?php

namespace Admin\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class CodigoPlugin extends AbstractPlugin {

    public function generarCodigo($xCodigo, $prefijo = null) {
        $posicion = strpos($xCodigo, "_");
        $numero = substr($xCodigo, $posicion + 1) + 1;
        $ceros = "";

        switch ($numero):
            case ($numero <= 9):
                $ceros = "00000";
            break;

            case ($numero <= 99):
                $ceros = "0000";
            break;

            case ($numero <= 999):
                $ceros = "000";
            break;

            case ($numero <= 9999):
                $ceros = "00";
            break;

            case ($numero <= 99999):
                $ceros = "0";
            break;

        endswitch;

        if (isset($prefijo) && !empty($prefijo)) {
            $codigo = $prefijo . "_" . $ceros . $numero;
        } else {
            $codigo = "id" . "_" . $ceros . $numero;
        }

        return $codigo;
    }

    public function codigoAleatorio($texto){
           $s = strtoupper(md5(uniqid(rand(),true))); 
           $guidText =$texto."-". 
           substr($s,0,2) . 
           substr($s,8,4); 
           return $guidText;
    }
    

}

?>
