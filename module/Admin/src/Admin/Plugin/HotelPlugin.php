<?php

namespace Admin\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class HotelPlugin extends AbstractPlugin {

	public function validaUrlComment($url,$web){

		$result="";

		$validaUrl= $this->urlExists($url);

		if($validaUrl){

			$html = $this->curlUrl($url);
            $valida = 0;
			switch ($web) {
				case 'Booking':
					if($html->find('ul[id=simple_comments_list]',0) || $html->find('.review_list',0)){
						$valida = 1;	
					}

				break;

				case 'Orbitz':
					if($html->find('.hotelRecentReviews',0)){
						$valida = 1;
					}
				break;

				case 'Despegar':
					if($html->find('div[id=common-comments-list]',0)){
						$valida = 1;
					}
				break;

				case 'Hotels':
					if($html->find('.reviews',0)){
						$valida = 1;
					}
				break;

				case 'Expedia':
					if($html->find('#hotel-name',0)){
						$valida = 1;
					}
					
				break;

			}

			if($valida !=0){
				$result = '<button class="btn btn-info btn-circle" type="button"><i class="fa fa-check"></i>
				</button>';
			}else{
				$result = '<button class="btn btn-warning btn-circle" type="button"><i class="fa fa-times"></i>
				</button>';
			}

		}else{
			$result = '<button class="btn btn-warning btn-circle" type="button"><i class="fa fa-times"></i>
			</button>';

		}

		return $result;
	}

	public function urlExists($url) {

		$pattern='|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i';
		if(preg_match($pattern, $url) > 0){

			$h = get_headers($url);
			$status = array();
			preg_match('/HTTP\/.* ([0-9]+) .*/', $h[0] , $status);
			return ($status[1] == 200);

		}else{
			return false;
		} 
	}

    public function curlUrl($url,$html = true) {
        $ch = curl_init();
        $timeout = 100;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
       // curl_setopt($ch, CURLOPT_HEADER, true); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $header = array();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] =  "Cache-Control: max-age=0";
        $header[] =  "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($ch, CURLOPT_FAILONERROR, 1); // stop when it encounters an error
        curl_setopt($ch, CURLOPT_COOKIESESSION, true );

        curl_setopt($ch, CURLOPT_USERAGENT,  $_SERVER['HTTP_USER_AGENT'] );
        curl_setopt($ch, CURLOPT_AUTOREFERER,true);

        $data = curl_exec($ch);

        curl_close($ch);

        if($html){
            $dom = new \simple_html_dom();
            // Load HTML from a string
            $dom->load($data);
        
            return $dom;
        }else{
            return $data;
        }
    }

    public function calculaPromedio($data){
        $bookingPuntaje = 0;
        $despegarPuntaje = 0;
        $expediaPuntaje = 0;
        $hotelsPuntaje = 0;
        $orbitzPuntaje = 0;

        $total = 0;

    	if($data['booking']!=""){
    		$bookingPuntaje = $this->capturaBooking($data['booking']);
            $total +=1;
    	}

    	if($data['expedia']!=""){
    		$expediaPuntaje = $this->capturaExpedia($data['expedia']);
            $total +=1;
    	}
        
    	if($data['despegar']!=""){
    		$despegarPuntaje = $this->capturaDespegar($data['despegar']);
            $total +=1;
    	}

    	if($data['hotels']!=""){
    		$hotelsPuntaje = $this->capturaHotels($data['hotels']);
            $total +=1;
    	}

    	if($data['orbitz']!=""){
    		$orbitzPuntaje = $this->capturaOrbitz($data['orbitz']);
            $total +=1;
    	}


    	$puntaje = $bookingPuntaje + $expediaPuntaje + $despegarPuntaje + $orbitzPuntaje + $hotelsPuntaje;
     
        $promedio = $puntaje / $total;

        $promedio = number_format($promedio, 1, ',', ' '); 


        return $promedio;
    }

    public function capturaBooking($url){

    	$html = $this->curlUrl($url);

        $puntaje = $html->find('#review_list_main_score',0)->plaintext;
       
        return $puntaje;
    }

    public function capturaDespegar($url){

    	$html = $this->curlUrl($url);

        $puntaje = 0;

        if($html->find('.ux-common-score-rate')){

            $puntaje = $html->find('.ux-common-score-rate span',0)->plaintext;

            $puntaje = str_replace("puntos","",$puntaje);

        }else{

            $i = 0;
            foreach ($html->find('div[id=common-comments-list]') as $d):
            foreach ($d->find('.ux-common-comment-bubble') as $e):
                if ($e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-best', 0)
                        || $e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-opinion-worst', 0)
                        || $e->find('.ux-common-comment-bubble-colopinion .ux-common-comment-bubble-colopinion-content .ux-common-comment-bubble-description', 0)) {

                    $i+=1;
                    $puntaje+= str_replace("puntos", "", $e->find('.ux-common-comment-bubble-rate', 0)->plaintext);

                }
    
            endforeach;

            endforeach;

            $puntaje = $puntaje / $i;

        }
       
        return $puntaje;
    }

    public function capturaExpedia($url){

        $html = $this->curlUrl($url);

        $puntaje = $html->find('.rating-number',0)->plaintext;
        $puntaje = $puntaje * 2;
       
        return $puntaje;
    }

    public function capturaHotels($url){

        $id = $this->dataHotels($url);
        $urlNew="http://es.hoteles.com/hotel/details.html?".$id;

        $html = $this->curlUrl($urlNew);

        $puntaje = $html->find('#hotel-reviews-summary .score-summary .rating > b',0)->plaintext;
        //$puntaje = $html->find('.rating .average',0)->plaintext;

        $puntaje = $puntaje * 2;
       
        return $puntaje;
    }

    public function capturaOrbitz($url){

        $html = $this->curlUrl($url);

        $puntaje = $html->find('.reviewRatingBubble',0)->plaintext;
        $puntaje = $puntaje * 2;
       
        return $puntaje;
    }

    public function obtenerUrlNew($url,$web,$idioma) {
    
        $urlNew = "";

        switch ($web) {
                case 'Booking':

                    $dataBook = $this->dataBooking($url);
                
                    $idiomaUrl = $this->capturaIdiomaUrl($idioma, "Booking");

                    $urlNew = $idiomaUrl."pagename=".$dataBook['name'].";cc1=".$dataBook['pais'].";type=total;dist=1;offset=0;rows=5;rid=;sort=f_recent_desc";

                    break;
                case 'Orbitz':

                    $idiomaUrl = $this->capturaIdiomaUrl($idioma, "Orbitz");
                    $cadena = $this->dataOrbitz($url);

                    $urlNew =  $idiomaUrl . $cadena;

                    break;

                case 'hotels':
                    $pos = strpos($url, "hoteles.com");
                    if ($pos != true) {
                        $pos = strpos($url, "hotels");
                    }

                    $cadena = substr($url, $pos);
                    $cadenaFormat = str_replace(["hoteles.com", "hotels.com"], "", $cadena);

                    $idiomaUrl = $this->capturaIdiomaUrl($idioma, "hotels");

                    $urlNew = $idiomaUrl . $cadenaFormat;

                    break;

                case 'Expedia':
                   $id = $this->dataExpedia($url);

                   $idiomaUrl = $this->capturaIdiomaUrl($idioma, "Expedia");

                   $urlNew = "http://reviewsvc.expedia.com/reviews/v1/retrieve/getReviewsForHotelId/".$id."/?_type=json&start=0&items=10&sortBy=&categoryFilter=&languageFilter=&languageSort=".$idiomaUrl."&pageName=page.Hotels.Infosite.Information";

                break;

                default:
                    # code...
                    break;
            }
        
        return $urlNew;
    }

    public function capturaIdiomaUrl($idioma, $web) {
        $prefijoUrl = "";
        switch ($web) {
            case 'Booking':
                switch ($idioma) {
                    case 'en_US':
                        $prefijoUrl = "http://www.booking.com/reviewlist.en.html?";
                        break;
                    case 'es_ES':
                        $prefijoUrl = "http://www.booking.com/reviewlist.es.html?";
                        break;
                }

                break;
            case 'Orbitz':
                switch ($idioma) {
                    case 'en_US':
                        $prefijoUrl = "http://www.orbitz.com/shop/hotelsearch?prefLang=en&hsv.showDetails=true&type=hotel&hotel.type=keyword&search=Search&hotel.hid=";
                        break;
                    case 'es_ES':
                        $prefijoUrl = "http://www.orbitz.com/shop/hotelsearch?prefLang=es&hsv.showDetails=true&type=hotel&hotel.type=keyword&search=Search&hotel.hid=";
                        break;

                }

                break;

            case 'hotels':
                switch ($idioma) {
                    case 'en_US':
                        $prefijoUrl = "http://www.hotels.com";
                        break;
                    case 'es_ES':
                        $prefijoUrl = "http://es.hotels.com";
                        break;
                }
                break;

            case 'Expedia':

            switch ($idioma) {
                case 'en_US':
                    $prefijoUrl = "en";
                    break;

                 case 'es_ES':
                    $prefijoUrl = "es";
                    break;
                
                default:
                    $prefijoUrl = "es";
                    break;
            }
            break;

            default:
                # code...
                break;
        }

        return $prefijoUrl;
    }

    public function dataBooking($url){
        $formateaUrl = str_replace("http://www.booking.com/hotel/", "", $url);
                 
        $pos = strpos($formateaUrl, ".");
        $cadena = substr($formateaUrl, $pos);
        $cadenaFormat = str_replace($cadena, "", $formateaUrl);

        $data['name'] = substr($cadenaFormat,3);
        $data['pais'] = substr($cadenaFormat, 0,2);

        return $data;
    }

    public function dataExpedia($url){
        $pos = strpos($url ,".h");
        $id = substr($url, $pos,9);
        $id = str_replace(".h","", $id);

        return $id;
    }

    public function dataHotels($url){

        $pos = strpos($url ,"hotelId");
        $id = substr($url, $pos,14);
       // $id = str_replace("hotelId=","", $id);

        return $id;
    }

    public function dataOrbitz($url){

        $pos = strpos($url ,".h");
        $id = substr($url, $pos);
        $id = str_replace(["h",".","/"],"", $id);

        return $id;
    }

}

?>