<?php

namespace Admin\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class UrlPlugin extends AbstractPlugin {

    public function create($string, $unique = false) {
        if ($unique === true) {
            $uri = $this->_clear($string) . '-' . $this->_uniqid();
        } else {
            $uri = $this->_clear($string);
        }
        return $uri;
    }

    private function _clear($string) {

        $stringX = $this->reemplazacadena($string);
        $stringY = strtolower($stringX);

        $stringF = preg_replace(array('~[^0-9a-z]~i', '~[ -]+~'), '-', $stringY);
        return trim($stringF, ' -');
    }

    private function _uniqid() {
        return substr(uniqid(), strlen(uniqid()) - 4);
    }

    private function reemplazacadena($nom_archivo) {
        $arr_busca = array(' ', 'á', 'à', 'â', 'ã', 'ª', 'Á', 'À',
            'Â', 'Ã', 'é', 'è', 'ê', 'É', 'È', 'Ê', 'í', 'ì', 'î', 'Í',
            'Ì', 'Î', 'ò', 'ó', 'ô', 'õ', 'º', 'Ó', 'Ò', 'Ô', 'Õ', 'ú',
            'ù', 'û', 'Ú', 'Ù', 'Û', 'ç', 'Ç', 'Ñ', 'ñ');
        $arr_susti = array('-', 'a', 'a', 'a', 'a', 'a', 'A', 'A',
            'A', 'A', 'e', 'e', 'e', 'E', 'E', 'E', 'i', 'i', 'i', 'I', 'I',
            'I', 'o', 'o', 'o', 'o', 'o', 'O', 'O', 'O', 'O', 'u', 'u', 'u',
            'U', 'U', 'U', 'c', 'C', 'N', 'n');
        $nom_archivo = trim(str_replace($arr_busca, $arr_susti, $nom_archivo));
        return preg_replace('/[^A-Za-z0-9\_\.\-]/', '', $nom_archivo);
    }

}
