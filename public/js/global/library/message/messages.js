/*
 *Control de Flash Messages para JQUERY
 **/
    var flashMsg = {
       init : function(){
            var mensajes = $('a[data-dismiss=alert]'),
            h = 0,
            s = 'middle',
            interval = '2000';
            $.each(mensajes, function(k, v){
                h = 1000 * (k);
                setTimeout(function(){
                    $(v).fadeIn(s, h, function(){
                        setTimeout(function(){
                            $(v).parent().fadeOut(s, function(){
                                $(this).remove();
                            });
                        }, h + interval);
                    });
                },h);
            });
        },
        show : function(contenedor, msg, tipo) {
            var html = "<div class='fade in alert alert-"+tipo+"'>"+
                       "<a class='close' data-dismiss='alert' href='#'>×</a>"+
                       "<h4 class='alert-heading'>"+msg+"</h4>"+
                       "</div>";
                   
            $(contenedor).prepend(html);
            flashMsg.init();
        }
    };
    