$(function() {

	var vConfig = {
		init : function(){

			$("#btn-procesar").click(function(){
				vConfig.crearHotelData();
				vConfig.crearPaisData();
				vConfig.crearCiudadData();
				vConfig.crearLocalidadData();
			})

		},
		crearHotelData : function(){
			$.ajax({
          		url:"/config/crear-data-hotel",
          		type:'POST',
          		async:true,
          		beforeSend:function(){
					var loading="<button class='btn btn-primary btn-circle' type='button'><i class='fa fa-spinner fa-spin'></i></button>";
            		$("#l-hotel").html(loading);
          		},
          		success:function(respuesta){
      	            $("#l-hotel").html("");
            		$("#hotel-data").val(respuesta);
          		}
        	});
		},
		crearPaisData : function(){
			$.ajax({
          		url:"/config/crear-data-pais",
          		type:'POST',
          		async:true,
          		beforeSend:function(){
					var loading="<button class='btn btn-primary btn-circle' type='button'><i class='fa fa-spinner fa-spin'></i></button>";
            		$("#l-pais").html(loading);
          		},
          		success:function(respuesta){
      	            $("#l-pais").html("");
            		$("#pais-data").val(respuesta);
          		}
        	});
		},
		crearCiudadData : function(){
			$.ajax({
          		url:"/config/crear-data-ciudad",
          		type:'POST',
          		async:true,
          		beforeSend:function(){
					var loading="<button class='btn btn-primary btn-circle' type='button'><i class='fa fa-spinner fa-spin'></i></button>";
            		$("#l-ciudad").html(loading);
          		},
          		success:function(respuesta){
      	            $("#l-ciudad").html("");
            		$("#ciudad-data").val(respuesta);
          		}
        	});
		},
		crearLocalidadData : function(){
			$.ajax({
          		url:"/config/crear-data-localidad",
          		type:'POST',
          		async:true,
          		beforeSend:function(){
					var loading="<button class='btn btn-primary btn-circle' type='button'><i class='fa fa-spinner fa-spin'></i></button>";
            		$("#l-localidad").html(loading);
          		},
          		success:function(respuesta){
      	            $("#l-localidad").html("");
            		$("#localidad-data").val(respuesta);
          		}
        	});
		}
	}

	vConfig.init();
})