$(function() {
  var vHotel = {
    init : function(){
      vHotel.subirImagen();
      vHotel.validarUrl();
      vHotel.enviarDatos();
      vHotel.eliminarImagen();

      $("#id_pais").change(function(){
         vHotel.listarCiudad();
      })

      $("#btn-promedio").click(function(){
         vHotel.calcularPromedio();
      })


    },
    calcularPromedio : function(){
        var vBooking = $("#booking_url").val();
        var vExpedia = $("#expedia_url").val();
        var vDespegar = $("#despegar_url").val();
        var vHotels = $("#hoteles_url").val();
        var vOrbitz = $("#orbitz_url").val();

        _Booking = "booking="+vBooking;
        _Despegar = "despegar="+vDespegar;
        _Expedia = "expedia="+vExpedia;
        _Hotels = "hotels="+vHotels;
        _Orbitz = "orbitz="+vOrbitz;

        var vData =_Booking+"&"+_Despegar+"&"+_Expedia+"&"+_Hotels+"&"+_Orbitz;

        $.ajax({
          url:"/hotel/promedio",
          type:'POST',
          async:true,
          data: vData,
          beforeSend:function(){

            var loading="<button class='btn btn-primary btn-circle' type='button'><i class='fa fa-spinner fa-spin'></i></button>";
            $("#lp").html(loading);

          },
          success:function(respuesta){

            $("#lp").html("");
            $("#puntaje").val(respuesta);

          }
        });

    },
    subirImagen : function(){
      var button = $("#btn_foto");
      new AjaxUpload("#btn_foto", {
       action: "/hotel/upload-image",
       onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
              // extensiones permitidas
              alert('Error: Solo se permiten imagenes');
              // cancela upload
              return false;
            } else {

              this.disable();
            }
          },
          onComplete: function(file, response){
          // habilito upload button                       
          this.enable();          
          // Agrega archivo a la lista
          vHotel.agregarImagen(response);

        }   
      });
    },
    agregarImagen : function(imagen){
      var htmlResponse = "";
      var urlImagen = "/img/hoteles/"+imagen;
      var imageF="<img src="+urlImagen+" width='80' />"

      htmlResponse +="<tr><td data-title='Foto'>"+imageF+"</td>";
      htmlResponse +="<td><div class='btn-group'><button data-toggle='dropdown' class='btn btn-warning btn-sm dropdown-toggle'>Action <span class='caret'></span></button><ul class='dropdown-menu'><li><a href='javascript:void(0)' class='eliminarF' ><i class='fa fa-times'></i> Eliminar</a></li></ul></div></td>";
      htmlResponse +="<td style='display:none'>"+imagen+"</td>";
      htmlResponse +="</tr>";
      $("#tb_fotos_hotel").append(htmlResponse);

      vHotel.eliminarImagen();

    },
    eliminarImagen : function(){
      $(".eliminarF").click(function() {
        var tr = $(this).closest('tr');
        //tr.css("background-color","#FF3700");
        tr.fadeOut(400, function(){
          tr.remove();
        });

      });
    },
    validarUrl : function(){

      $(".url_validation").change(function(){
        var web = $(this).attr("data-web");
        var index = $(this).attr("data-index");
        var url = $(this).get(0).value;

        _Web = "web="+web;
        _Url = "url="+url;

        var datos=_Web+"&"+_Url;

        $.ajax({
          url:"/hotel/url-comment-valid",
          type:'POST',
          async:true,
          data: datos,
          beforeSend:function(){

            var loading="<button class='btn btn-primary btn-circle' type='button'><i class='fa fa-spinner fa-spin'></i></button>";
            $(".res-url-"+index).html(loading);

          },
          success:function(respuesta){

            $(".res-url-"+index).html(respuesta);

          }
        });

      })

    },

    enviarDatos : function(){
        $('#frmHotel').submit(function(e) {
            vHotel.capturaFoto();
        })
    },
    capturaFoto : function(){
        var totalFilas = $("#tb_fotos_hotel tr").length;
        if(totalFilas >0){
          var foto = new Array();
          var i = 0;
          var htmlResponse = "";
          $("#tb_fotos_hotel tr").each(function(){
             foto[i] = $(this).find("td").eq(2).html();
             i+=1;
          });

          htmlResponse ="<input type='hidden' name='foto' id='foto' value="+foto+" >";
          $("#tb_fotos_hotel").append(htmlResponse);
        }

    },
    listarCiudad : function(){
        var vPais = vHotel.capturaDatosSelect("id_pais");

        _Id = "id="+vPais;

        $.ajax({
          url:"/hotel/ajax-ciudad",
          type:'POST',
          async:true,
          data: _Id,
          beforeSend:function(){

          },
          success:function(respuesta){

            $("#id_ciudad").html(respuesta);

          }
        });

    },
    capturaDatosSelect : function(vElement){
        var vIndex = $("#"+vElement).get(0).selectedIndex;
        var vValue = $("#"+vElement).get(0).options[vIndex].value;

        return vValue;
    }
  }

  vHotel.init();
});


