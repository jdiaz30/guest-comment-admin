(function($){
    
$.fn.extend({  

    validar_email: function(){
        
        this.each(function() {
            var vObjeto=$(this);
            
         
            vObjeto.change(function(){
                 
                  // creamos nuestra regla con expresiones regulares.
                  var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
                  // utilizamos test para comprobar si el parametro valor cumple la regla
                  if(filter.test(vObjeto.val()))
                     $(".control-group.correo").removeClass("error");
                  else
                     $(".control-group.correo").addClass("error");
              
            })
           
        })
     
    }
    
    
});
    
})(jQuery);
